<?php
require_once(t3lib_extMgm::extPath('templavoila').'pi1/class.tx_templavoila_pi1.php');

class user_mojokesearch_templavoila {
  /**
   * Pages to index
   * @var array
   */
  protected $indexPids = '';
  /**
   * Array containing data of all pages
   * @var array
   */
  protected $pageRecords;
  /**
   * Which content element types to index
   * @var array
   */
  protected $indexCTypes = array('templavoila_pi1', 'text', 'textpic', 'bullets', 'table', 'html');

  /**
   * Counter for indexed content elements
   * @var integer
   */
  protected $counter = 0;
  /**
   * content element specific where clause
   * @var string
   */
  protected $whereClauseForCType = '';
  /**
   * Status information for user
   * @var string
   */
  protected $content = '';
  /**
   * The indexer object
   * TODO: Find correct type
   */
  protected $indexerObject;
  /**
   * The indexer configuration
   * @var array
   */
  protected $indexerConfig;
  /**
   * Query generator
   * @var t3lib_queryGenerator
   */
  protected $queryGen;
  /**
   * Templavoila Renderer
   * @var tx_templavoila_pi1
   */
  protected $tvObj;

  /**
   * Custom index for ke_search to index content provided
   * by the extension templavoila
   *
   * @param   array $indexerConfig
   * @param   array $indexerObject
   * @return  string $output
   * @author  Morton Jonuschat <yabawock@gmail.com>
   */
  public function customIndexer(&$indexerConfig, &$indexerObject) {
    # bail out if we are passed a config that is not of our type
    if($indexerConfig['type'] !== 'templavoila') {
      return FALSE;
    }

    # SQL Safe search params
    foreach($this->indexCTypes as $key => $cType) {
      $this->indexCTypes[$key] = $GLOBALS['TYPO3_DB']->fullQuoteStr($cType, 'tt_content');
    }
    $this->whereClauseForCType = 'CType IN (' . implode(',', $this->indexCTypes) . ')';

    # Set the passed indexer configuration as default
    $this->indexerConfig = $indexerConfig;
    $this->indexerObject = $indexerObject;
    $this->tvObj = t3lib_div::makeInstance('tx_templavoila_pi1');
    $this->tvObj->cObj = t3lib_div::makeInstance('tslib_cObj');
    $this->queryGen = t3lib_div::makeInstance('t3lib_queryGenerator');

    # Make more parts of the frontend available (needed for templavoila)
    $GLOBALS['TT'] = new t3lib_timeTrackNull;
    $GLOBALS['TSFE'] = t3lib_div::makeInstance('tslib_fe', $GLOBALS['TYPO3_CONF_VARS'], 1, 0);
    $GLOBALS['TSFE']->sys_page = t3lib_div::makeInstance('t3lib_pageSelect');
    $GLOBALS['TSFE']->sys_page->init(TRUE);
    $GLOBALS['TSFE']->initTemplate();

    foreach(t3lib_div::trimExplode(',', $this->indexerConfig['startingpoints_recursive'], true) as $pid) {
      $GLOBALS['TSFE']->rootLine =  $GLOBALS['TSFE']->sys_page->getRootLine($pid, '');
      $GLOBALS['TSFE']->getConfigArray();

      # Define the search scope
      $this->getPagelist($pid);
      $this->getPageRecords();
      $this->addTagsToPageRecords();
      foreach($this->indexPids as $uid) {
        $this->getPageContent($uid);
      }
    }
    $this->content .= '<p><b>Indexer "' . $this->indexerConfig['title'] . '": ' . count($this->pageRecords) . ' pages have been found for indexing.</b></p>' . "\n";
    $this->content .= '<p><b>Indexer "' . $this->indexerConfig['title'] . '": ' . $this->counter . ' pages have been indexed.</b></p>' . "\n";
    # TODO: add showTime from ke_search
    # $this->content .= $this->showTime();

    return $this->content;
  }

  /**
   * get all recursive contained pids of given Page-UID
   *
   * @author	Andreas Kiefer (kennziffer.com) <kiefer@kennziffer.com>
   * @author	Stefan Froemken (kennziffer.com) <froemken@kennziffer.com>
   * @author  Morton Jonuschat <yabawock@gmail.com>
   * @package	tx_kesearch
   * @return  void
   */
  function getPagelist($pid) {
    # index only pages of doktype standard, advanced and "not in menu"
    $where = ' doktype IN (1,2,4,5,254) ';
    # index only pages which are searchable and not hidden
    # deleted clause is integrated in getTreeList. So don't add this here.
    $where .= ' AND no_search <> 1 AND hidden=0';

    # if indexing of content elements with restrictions is not allowed
    # get only pages that have empty group restrictions
    if($this->indexerConfig['index_content_with_restrictions'] != 'yes') {
      $where .= ' AND (fe_group = "" OR fe_group = "0") ';
    }

    $pageList = $this->queryGen->getTreeList($pid, 99, 0, $where);
    $this->indexPids = t3lib_div::trimExplode(',', $pageList);
  }

  /**
   * get array with all pages
   *
   * @author	Andreas Kiefer (kennziffer.com) <kiefer@kennziffer.com>
   * @author	Stefan Froemken (kennziffer.com) <froemken@kennziffer.com>
   * @author  Morton Jonuschat <yabawock@gmail.com>
   * @package tx_kesearch
   * @return  void
   */
  protected function getPageRecords() {
    $this->pageRecords = array();
    $fields = '*';
    $table = 'pages';
    $where = 'doktype NOT IN (4,254) AND uid IN (' . implode(',', $this->indexPids) . ')';
    $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($fields, $table, $where);

    while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
      $this->pageRecords[$row['uid']] = $row;
    }
  }

  /**
   * get content of current page and save data to db
   * @param $uid page-UID that has to be indexed
   */
  protected function getPageContent($uid) {
    # get content elements for this page
    $fields = '*';
    $table = 'tt_content';
    $where = 'pid = ' . intval($uid);
    $where .= ' AND ' . $this->whereClauseForCType;
    $where .= t3lib_BEfunc::BEenableFields($table);
    $where .= t3lib_BEfunc::deleteClause($table);

    # if indexing of content elements with restrictions is not allowed
    # get only content elements that have empty group restrictions
    if($this->indexerConfig['index_content_with_restrictions'] != 'yes') {
      $where .= ' AND (fe_group = "" OR fe_group = "0") ';
    }

    $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($fields, $table, $where);

    # Return if there are no indexable content elements
    if(count($rows) === 0) {
      return;
    }

    # Additional fields for the indexer
    $additionalFields = array(
      'sortdate' => $this->pageRecords[$uid]['tstamp'],
      'orig_uid' => $this->pageRecords[$uid]['uid'],
      'orig_pid' => $this->pageRecords[$uid]['pid'],
    );

    # Render the content elements on this page
    $pageContent = array();
    foreach($rows as $row) {
      # Header / Title
      $pageContent[$row['sys_language_uid']] .= strip_tags($row['header']) . "\n";

      # Update the page timestamp  to reflect the newest element on the page
      if($additionalFields['sortdate'] < $row['tstamp']) {
        $additionalFields['sortdate'] = $row['tstamp'];
      }

      # Bodytext
      if($row['CType'] == 'templavoila_pi1') {
        $this->tvObj->cObj->data = $row;
        $bodytext = $this->tvObj->renderElement($row, 'tt_content');
      } else {
        $bodytext = $row['bodytext'];
      }
      $bodytext = str_replace('<td', ' <td', $bodytext);
      $bodytext = str_replace('<br', ' <br', $bodytext);
      $bodytext = str_replace('<p', ' <p', $bodytext);
      $bodytext = strip_tags($bodytext);

      # Nicen up table content
      if ($row['CType'] == 'table') {
        # replace table dividers with whitespace
        $bodytext = str_replace('|', ' ', $bodytext);
      }
      $pageContent[$row['sys_language_uid']] .= $bodytext."\n";
    }

    # get Tags for current page
    $tags = $this->pageRecords[intval($uid)]['tags'];
    if(is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['ke_search']['modifyPagesIndexEntry'])) {
      foreach($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['ke_search']['modifyPagesIndexEntry'] as $_classRef) {
        $_procObj = & t3lib_div::getUserObj($_classRef);
        $_procObj->modifyPagesIndexEntry(
          $this->pageRecords[$uid]['title'],
          $pageContent,
          $tags,
          $this->pageRecords[$uid],
          $additionalFields
        );
      }
    }

    # store record in index table
    foreach($pageContent as $langKey => $content) {
      $this->indexerObject->storeInIndex(
        $this->indexerConfig['storagepid'],     # storage PID
        $this->pageRecords[$uid]['title'],      # page title
        'page',                                 # content type
        $uid,                                   # target PID: where is the single view?
        $content,                               # indexed content, includes the title (linebreak after title)
        $tags,                                  # tags
        '',                                     # typolink params for singleview
        '',                                     # abstract
        $langKey,                               # language uid
        $this->pageRecords[$uid]['starttime'],  # starttime
        $this->pageRecords[$uid]['endtime'],    # endtime
        $this->pageRecords[$uid]['fe_group'],   # fe_group
        false,                                  # debug only?
        $additionalFields                       # additional fields added by hooks
      );
    }
    $this->counter++;
  }

  /**
   * Add Tags to pages array
   * @author	Andreas Kiefer (kennziffer.com) <kiefer@kennziffer.com>
   * @author	Stefan Froemken (kennziffer.com) <froemken@kennziffer.com>
   * @author  Morton Jonuschat <yabawock@gmail.com>
   * @package tx_kesearch
   * @return void
   */
  protected function addTagsToPageRecords() {
    $tagChar = $this->indexerObject->extConf['prePostTagChar'];
    # add tags which are defined by page properties
    $fields = 'pages.*, GROUP_CONCAT(CONCAT("' . $tagChar . '", tx_kesearch_filteroptions.tag, "' . $tagChar . '")) as tags';
    $table = 'pages, tx_kesearch_filteroptions';
    $where = 'pages.uid IN (' . $this->indexPids . ')';
    $where .= ' AND pages.tx_kesearch_tags <> "" ';
    $where .= ' AND FIND_IN_SET(tx_kesearch_filteroptions.uid, pages.tx_kesearch_tags)';
    $where .= t3lib_befunc::BEenableFields('tx_kesearch_filteroptions');
    $where .= t3lib_befunc::deleteClause('tx_kesearch_filteroptions');

    $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($fields, $table, $where, 'pages.uid', '', '');
    while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
      $this->pageRecords[$row['uid']]['tags'] = $row['tags'];
    }

    # add tags which are defined by filteroption records
    $fields = 'automated_tagging, tag';
    $table = 'tx_kesearch_filteroptions';
    $where = 'automated_tagging <> "" ';
    $where .= t3lib_befunc::BEenableFields('tx_kesearch_filteroptions');
    $where .= t3lib_befunc::deleteClause('tx_kesearch_filteroptions');

    $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($fields, $table, $where);

    # index only pages of doktype standard, advanced and "not in menu"
    $where = ' (doktype = 1 OR doktype = 2 OR doktype = 5) ';
    # index only pages which are searchable
    $where .= ' AND no_search <> 1 ';

    foreach($rows as $row) {
      $tempTags = array();
      $pageList = t3lib_div::trimExplode(',', $this->queryGen->getTreeList($row['automated_tagging'], 99, 0, $where));
      foreach($pageList as $uid) {
        if($this->pageRecords[$uid]['tags']) {
          $this->pageRecords[$uid]['tags'] .= ',' . $tagChar . $row['tag'] . $tagChar;
        } else {
          $this->pageRecords[$uid]['tags'] = $tagChar . $row['tag'] . $tagChar;
        }
      }
    }
  }
}
