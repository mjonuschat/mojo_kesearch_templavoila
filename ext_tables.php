<?php
if (!defined ('TYPO3_MODE')) {
  die ('Access denied.');
}

# Add custom indexer to ke_search
t3lib_div::loadTCA('tx_kesearch_indexerconfig');

$TCA['tx_kesearch_indexerconfig']['columns']['type']['config']['items'][] = array (
  'LLL:EXT:mojo_kesearch_templavoila/Resources/Private/Language/locallang_db.xml:tx_mojokesearch_templavoila.indexer_name',
  'templavoila',
  t3lib_extMgm::extRelPath('mojo_kesearch_templavoila') . 'Resources/Public/Icons/selicon_indexer_templavoila.gif',
);

$TCA['tx_kesearch_indexerconfig']['columns']['targetpid']['displayCond'] .= ',templavoila';
$TCA['tx_kesearch_indexerconfig']['columns']['startingpoints_recursive']['displayCond'] .= ',templavoila';
$TCA['tx_kesearch_indexerconfig']['columns']['single_pages']['displayCond'] .= ',templavoila';
$TCA['tx_kesearch_indexerconfig']['columns']['index_content_with_restrictions']['displayCond'] .= ',templavoila';
