<?php

########################################################################
# Extension Manager/Repository config file for ext "mojo_kesearch_templavoila".
#
# Auto generated 20-07-2011 16:45
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Templavoila Indexer für ke_search',
	'description' => 'Diese Erweiterung ergänzt ke_search um die Fähigkeit Seiten mit Templavoila Inhaltselementen zu indexieren.',
	'category' => 'be',
	'author' => 'Morton Jonuschat',
	'author_email' => 'yabawock@gmail.com',
	'shy' => '',
	'dependencies' => 'ke_search',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'MoJo Code',
	'version' => '0.5.0',
	'constraints' => array(
		'depends' => array(
			'ke_search' => '0.4.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:5:{s:9:"ChangeLog";s:4:"8570";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"1bdc";s:19:"doc/wizard_form.dat";s:4:"8e01";s:20:"doc/wizard_form.html";s:4:"1943";}',
);

?>
